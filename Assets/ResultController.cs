﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ResultController : MonoBehaviour
{
    public Text toRead;
    public Text toSet;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadText()
    {
        toSet.text = GameManagerController.instance.text;
        Debug.Log("Text loaded! " + GameManagerController.instance.text);
    }

    public void SaveText()
    {
        GameManagerController.instance.text = toRead.text;
        Debug.Log("Text saved! " + GameManagerController.instance.text);
    }

    public void ChangeScene(string newSceneName)
    {
        SceneManager.LoadScene(newSceneName); 
    }
}
