﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerController : MonoBehaviour
{
    public static GameManagerController instance = null;
    public string text = "";

    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
            Debug.Log("GameManager initiated. " + GetInstanceID().GetHashCode());
        }
        else
        {
            Destroy(this);
            Debug.Log("GameManager already exists. Deleting this new one... " + GetInstanceID().GetHashCode());
        }
    }

    public void SetText(string text)
    {
        instance.text = text;
    }

    public string GetText()
    {
        return instance.text;
    }
}
